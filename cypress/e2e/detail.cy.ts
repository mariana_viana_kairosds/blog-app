it("visit Posts", () => {
  cy.visit("http://localhost:5174/blog-app/post");
});

it("watch detail", () => {
cy.get(':nth-child(1) > :nth-child(3) > .btn-detail').click();
})

it('delete comment', () => {
  cy.get('.btn-delete-commt').click();
})

it("create comment", () => {
cy.get('#author').type("admin");
cy.get('#content').type("que gusta mucho este post");
cy.get('.btn-create-commt').click()
})

it('edit commt', () => {
  cy.get('.btn-edit-commt').click();
  cy.get('#content').type('anque podria mejorar un poco');
  cy.get('.btn-create-commt').click();
})
