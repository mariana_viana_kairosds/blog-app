it('go to post and get All Posts', ()=>{
  cy.visit('http://localhost:5174/blog-app/post');
  cy.get('.btn-create').click();
})

it('delete post', () => {
  cy.get('.btn-delete').click();
})

it('create Posts', ()=>{
  cy.get('#name').type('admin');
  cy.get('#title').type('las bellas rosas');
  cy.get('#content').type('las bellas rosas son hermosas por su color y olor, son tan bellas que solo duran un día, recordando su fragilidad');
  cy.get('.btn-send').click();
  cy.get('#name').clear();
  cy.get('#title').clear();
  cy.get('#content').clear();
})

it('edit post', () => {
  cy.get('.btn-edit').click();
  cy.get('#title').type('las feas rosas');
  cy.get('#content').type('las bellas rosas son hermosas por su color y olor, son tan bellas que solo duran un día, recordando su fragilidad');
  cy.get('.btn-send').click();
})
