it("login works with admin user", () => {
  cy.visit("https://local-apps.kairosds.com/blog-app/login");
  cy.get("#email").type("admin@gmail.com");
  cy.get("#pass").type("admin");
  cy.get("button").click();
});
