import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import { ConfigService } from './services/config/config-service';
import "./styles/App.css";

(async () => {
  await ConfigService.load();
  
ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <BrowserRouter basename='blog-app'>
    <App />
  </BrowserRouter>);
})();
