import { createContext, SetStateAction, useState } from "react";

export interface Role {
    role: string;
    setRole: React.Dispatch<SetStateAction<string>>
}

export const RoleContext = createContext<Role | undefined>(undefined);
RoleContext.displayName = "role";

export const RoleProvider = ({children}: any) => {
    const [role, setRole] = useState("");

    return (
        <RoleContext.Provider value={{role, setRole}}>
            {children}
        </RoleContext.Provider>
    )
}

function typeOf(typeOf: any, role: string) {
    throw new Error("Function not implemented.");
}
