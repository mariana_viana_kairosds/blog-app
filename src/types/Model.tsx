export interface ModelPost {
    id?: string;
    author?: string;
    title?: string;
    content?: string;
}

export interface ModelLogin {
    email: string;
    password: string;
}

export interface ModelSignUp {
    nickName: string;
    email: string;
    password: string;
    role: string;
}

export interface Token {
    token: string;
}

export interface PostDetailModel {
    id: string;
    title: string; 
    content: string;
    comments: CommentDetail
}

export interface CommentDetail {
    id?: string;
    content?: string
}

export interface CommentCreate {
    author?: string;
    content?: string;
}