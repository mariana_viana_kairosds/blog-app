import { Navigate, Route, Routes } from 'react-router-dom';
import { RoleProvider } from '../context/role-context';
import { Login } from '../pages/Login';
import { PostDetail } from '../pages/PostDetail';
import { PostsPage } from '../pages/Posts';
import { SignUp } from '../pages/SignUp';

export const Router = () => {
    return (<>
    <RoleProvider>
    <Routes>
        <Route path='*' element={<Navigate to='login'/>}/>
        <Route path='/login' element={<Login/>}/>
        <Route path='/sign-up' element={<SignUp/>}/>
        <Route path='/post' element={<PostsPage/>}/>
        <Route path='/post/:postId' element={<PostDetail/>}/>
    </Routes>
    </RoleProvider>

    </>)
}