import { Config } from './config';
import { ConfigProxyService } from './config-proxy';

export class ConfigService {
    static config: Config;

    static async load() {
        const service = new ConfigProxyService();
        const config: Config = await service.getConfig();
        this.config = config;
    }

    static get env(): Config {
        return this.config;
    }
}