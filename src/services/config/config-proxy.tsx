import axios from "axios";
import { Config } from "./config";


export class ConfigProxyService {
    async getConfig(): Promise<Config> {
        const response = await axios.get<Config>("/blog-app/config/config.json");
        return response.data;
    }
}
