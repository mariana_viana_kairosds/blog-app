import axios from 'axios';
import { CommentCreate, ModelPost } from '../types/Model';
import { ConfigService } from './config/config-service';

let token: any = "";

export const setTokenUser = (newToken: any) => {
  token = `Bearer ${newToken}`
}

export const CreatePostRepository = async (data: ModelPost) => {
  const response = await axios.post(`${ConfigService.env.api}/posts`, data, {
    headers: {
      "Content-Type": "application/json",
      'Authorization': token
    }
  })
  return response.data;
}

export const GetPostRepository = async () => {
  const response = await axios.get(`${ConfigService.env.api}/posts`, {
    headers: {
      "Content-Type": "application/json",
    }
  })
  return response.data;
}

export const GetPostRepositoryId = async (postId: string) => {
  const response = await axios.get(`${ConfigService.env.api}/posts/${postId}`, {
    headers: {
      "Content-Type": "application/json",
    }
  })
  return response.data;
}

export const DeletePostRepository = async (postId: string) => {
  const response = await axios.delete(`${ConfigService.env.api}/posts/${postId}`, {
    headers: {
      "Content-Type": "application/json",
      'Authorization': token
    }
  })
  return response.data;
}

export const UpdatePostRepository = async (postId: string, data: ModelPost) => {
  const response = await axios.patch(`${ConfigService.env.api}/posts/${postId}`,data , {
    headers: {
      "Content-Type": "application/json",
      'Authorization': token
    }
  })
  return response.data;
}

export const CreateComments = async (data: CommentCreate, postId: string) => {
  const response = await axios.post(
    `${ConfigService.env.api}/posts/${postId}/comment`,
    data,
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
    }
  );
  return response.data;
};

export const DeleteCommentRepository = async (postId: string,commentId: string) => {
  const response = await axios.delete(`${ConfigService.env.api}/posts/${postId}/comment/${commentId}`, {
    headers: {
      "Content-Type": "application/json",
      'Authorization': token
    }
  })
  return response.data;
}

export const UpdateCommentRepository = async (postId: string ,commentId: string ,data: CommentCreate) => {
  const response = await axios.patch(`${ConfigService.env.api}/posts/${postId}/comment/${commentId}`,data , {
    headers: {
      "Content-Type": "application/json",
      'Authorization': token
    }
  })
  return response.data;
}