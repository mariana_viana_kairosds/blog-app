import axios from "axios";
import { ModelLogin } from "../types/Model";
import { ConfigService } from "./config/config-service";

export const LoginRepository = async (credentials: ModelLogin) => {
  const { data } = await axios.post(`${ConfigService.env.api}/login`, credentials, {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return data;
};