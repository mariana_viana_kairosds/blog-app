import { describe, it } from "vitest";
import { CreatePostRepository } from "./post-repository";
/* eslint-disable import/first */
const responsePost = {
    author: "admin",
    content: "gnsfgnfn ghsfghshnh gfgdgfa gafgjhgfd ggasfg gjgf ghjgfg",
    id: "10b1d474-4b6a-4016-bf6a-aff315bc5e54",
    title: "el cuento",
};

vi.mock("./post-repository", () => {
  return {
    CreatePostRepository: vi.fn().mockImplementation(() => {
      return {
        responsePost
      };
    }),
  };
});

describe("Log in use case...", () => {
  it("should execute correctly, returning an access token", async () => {
    const response = await CreatePostRepository({
      author: "admin",
      content: "gnsfgnfn ghsfghshnh gfgdgfa gafgjhgfd ggasfg gjgf ghjgfg",
      title: "el cuento",
    });
    expect(response.responsePost.author).toEqual(responsePost.author);
    expect(response.responsePost.author).not.toBeNull();
  });
});
