import axios from "axios";
import { ModelSignUp } from "../types/Model";
import { ConfigService } from "./config/config-service";

export const SignUpRepository = async (credentials: ModelSignUp) => {
  const { data } = await axios.post(`${ConfigService.env.api}/sign-up`, credentials, {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return data;
};