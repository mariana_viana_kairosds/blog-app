import axios from "axios";
import { ConfigService } from "./config/config-service";

export const GetRole = async (token: string) => {
  const { data } = await axios.get(`${ConfigService.env.api}/auth/role/me`, {
    headers:{
      'Authorization': ` Bearer ${token}`
    }
  });
  
  return data;
};
