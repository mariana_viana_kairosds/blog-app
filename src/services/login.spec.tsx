import { describe, expect, it } from "vitest";
/* eslint-disable import/first */
   const login_Response = {
  token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsImlhdCI6MTY2MzMyMjIyNSwiZXhwIjoxNjYzNDA4NjI1fQ.zoCN6zEK0wNu5MDAs47ql3X4BRaMr6Vuuf8rSOIgCXQ",
};

vi.mock("./login", () => {
  return {
    LoginRepository: vi.fn().mockImplementation(() => {
      return {
          token: login_Response.token
      };
    }),
  };
});

import { LoginRepository } from "./login";

describe("Log in use case...", () => {
  it("should execute correctly, returning an access token", async () => {
    const response = await LoginRepository({
      email: "admin@gmail.com",
      password: "admin",
    });
    expect(response.token).toEqual(login_Response.token);
  });
});

