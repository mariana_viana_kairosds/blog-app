import { SignUpRepository } from "./sign-up";

vi.mock("./sign-up", () => {
  return {
    SignUpRepository: vi.fn().mockImplementation(() => {
      return {
          status: "Created"
      };
    }),
  };
});

describe("Sign Up use case...", () => {
  it("should execute correctly, returning an access token", async () => {
    const response = await SignUpRepository({
     nickName: "otro",
     email: "otro@gmail.com",
     password: "123456",
     role: "USER"
    });
    expect(response.status).toBe('Created');
  });
});