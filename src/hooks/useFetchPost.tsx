import { useEffect, useState } from "react";
import {
  CreatePostRepository,
  DeletePostRepository,
  GetPostRepository,
  UpdatePostRepository
} from "../services/post-repository";
import { ModelPost } from "../types/Model";
import { useNotification } from "./useNotification";

export const useFetchPost = () => {
  
  const [editing, setEditing] = useState<boolean>(false);
  const [posts, setPosts] = useState<ModelPost[]>([]);
  const [currentPost, setCurrentPost] = useState<any>([]);
  const [author, setAuthor] = useState<string>("");
  const [title, setTitle] = useState<string>("");
  const [content, setContent] = useState<string>("");
  const { setErrorMessage } = useNotification();
  const [clickButtonPost, setClickButtonPost] = useState(false);

  useEffect(() => {
    GetPostRepository().then((response) => setPosts(response));
  }, []);

  const handleTitle = (value: any) => {
    setTitle(value);
  };

  const handleContent = (value: any) => {
    setContent(value);
  };

  const handleUser = (value: any) => {
    setAuthor(value);
  };

  //create and update
  const createPost = async () => {
    try {
      const response = await CreatePostRepository({
        author: author,
        title: title,
        content: content,
      });
      setPosts([...posts, response]);
      setClickButtonPost(false);
      setAuthor("");
      setTitle("");
      setContent("");
    } catch (err: any) {
      setErrorMessage(err.response.data);
    }
  };

  //delete
  const deletePost = async (value: any) => {
    const id = value;
    try {
      await DeletePostRepository(id);
      const deletePost = posts.filter((post: ModelPost) => post.id !== id);
      setPosts(deletePost);
    } catch (err: any) {
      setErrorMessage(err.response.data);
    }
  };

  //update
  const updatePost = (value: any) => {
    const id = value;
    const findId: any = posts.filter((post: any) => post.id === id);
    setCurrentPost({
      id: findId[0].id,
      title: findId[0].title,
      content: findId[0].content,
    });
    setTitle(findId[0].title);
    setContent(findId[0].content);
    const findPost = posts.filter((post: any) => post.id !== id);
    setPosts(findPost);
    setClickButtonPost(true);
  };

  const updatePosts = async () => {
    try {
      const response = await UpdatePostRepository(currentPost.id, {
        title: title,
        content: content,
      });
      setPosts([...posts, response]);
      setEditing(false);
      setClickButtonPost(false);
      setTitle("");
      setContent("");
    } catch (err: any) {
      setErrorMessage(err.error);
    }
  };

  return {
    setEditing,
    editing,
    posts,
    author,
    title,
    content,
    handleTitle,
    handleContent,
    handleUser,
    createPost,
    deletePost,
    updatePost,
    updatePosts,
    clickButtonPost,
    setClickButtonPost
  };
};
