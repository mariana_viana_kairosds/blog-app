import { useEffect, useState } from "react";
import {
  CreateComments,
  DeleteCommentRepository,
  GetPostRepositoryId,
  UpdateCommentRepository
} from "../services/post-repository";
import { useFetchPost } from "./useFetchPost";

export const useComments = (location: any) => {
  const [comments, setComments] = useState<any>([]);
  const [author, setAuthor] = useState<string>("");
  const [content, setContent] = useState<string>("");
  const [currentComment, setCurrentComment] = useState<any>({});
  const { setEditing } = useFetchPost();
  const [message, setMessage] = useState<string>("");

  useEffect(() => {
    GetPostRepositoryId(location.postId).then((response) => {
      setComments(response.comments);
    });
  }, []);

  const handleContent = (value: any) => {
    setContent(value);
  };

  const handleUser = (value: any) => {
    setAuthor(value);
  };

  //create comment
  const createComment = async () => {
    try {
      const newContent = await CreateComments(
        { author: author, content: content },
        location.postId
      );
      setMessage("Comment Created");
      setComments([...comments, newContent]);
      setAuthor("");
      setContent("");
    } catch (err: any) {
      setMessage(err.response.data.error);
    }
  };

  const deleteComment = async (value: any) => {
    const id = value;
    try {
      await DeleteCommentRepository(location.postId, id);
      const deleteComment = comments.filter(
        (comment: any) => comment.id !== id
      );
      setComments(deleteComment);
      setMessage("deleted");
    } catch (err: any) {
      setMessage(err.error);
    }
  };

  const handleComment = (value: any) => {
    const id = value;
    const findId = comments.filter((comment: any) => comment.id === id);
    setContent(findId[0].content);
    setCurrentComment({
      id: findId[0].id,
      content: findId[0].content,
    });
    const findComment = comments.filter((comment: any) => comment.id !== id);
    setComments(findComment);
  };

  const updateComment = async () => {
    try {
      const response = await UpdateCommentRepository(
        location.postId,
        currentComment.id,
        { content: content }
      );
      setComments([...comments, response]);
      setEditing(false);
    } catch (err: any) {
      setMessage(err.error);
    }
  };

  return {
    comments,
    author,
    content,
    handleContent,
    updateComment,
    handleUser,
    handleComment,
    deleteComment,
    createComment,
    message
  };
};
