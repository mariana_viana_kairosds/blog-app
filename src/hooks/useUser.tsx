import { useState } from "react";

export const useUser = () => {

  const [token, setToken] = useState<string>('');
  const [signUp, setSignUp] = useState<string>('');

  return {setToken, token, setSignUp, signUp};
}