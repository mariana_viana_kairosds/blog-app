import { useEffect } from "react";
import { setTokenUser } from "../services/post-repository";
import { useUser } from "./useUser";

export const useToken = () => {

const {setToken} = useUser();

useEffect(() => {
  const logginJson = window.localStorage.getItem('tokenBlog');
  if(logginJson){
    const user = JSON.parse(logginJson);
    setToken(user);
    setTokenUser(user.token);
  }
}, [])

}