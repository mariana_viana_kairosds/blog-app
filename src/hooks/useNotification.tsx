import { useState } from "react";

export const useNotification = () => {

  const [errorMessage, setErrorMessage] = useState<string>('');

  return {setErrorMessage, errorMessage};
}