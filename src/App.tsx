import { useToken } from './hooks/useToken';
import { Router } from './routes/routes';
import './styles/App.css';

function App() {

  useToken();
  return (
    <div className="app">
      <header className='header'>
        <h1>Blog Posts</h1>
      </header>
       <Router/>
    </div>
  )
}

export default App
