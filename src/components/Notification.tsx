import { StyledMessage } from "./styled/generalStyled";

export const Notification = ({ message }: any) => {

   return (
      <StyledMessage>
        <p>{message}</p>
      </StyledMessage>
    );
};
