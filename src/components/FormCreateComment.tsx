import { Button } from "./styled/Button";
import { StyledForm, StyledInput, StyledLabel } from "./styled/generalStyled";

export const FormCreateComments = ({handleContent, handleUser, createComment, editing, updateComment, author, content}: any) => {

 const handleInputChange = (ev: any) => {
   ev.preventDefault();
 }
 
  const handleUserValue = (ev: any) => {
     handleUser(ev.target.value);
 }

   const handleContentValue = (ev: any) => {
     handleContent(ev.target.value);
 }

  return(
    <StyledForm onSubmit={handleInputChange}>
   <StyledLabel htmlFor="author">Author</StyledLabel>
    <StyledInput
        type='text'
        id="author"
        name="author"
        value={author}
        onChange={handleUserValue}
      />
    <StyledLabel htmlFor="content">Content</StyledLabel>
    <StyledInput
        type='text'
        id="content"
        name="content"
        value={content}
        onChange={handleContentValue}
      />
      <Button onClick={editing ? updateComment : createComment} className="btn-create-commt">{editing ? "Update" : "Create"}</Button>
    </StyledForm>
  )
}