import "../styles/Post.css";
import { Button } from "./styled/Button";

export const PostDetailComp = ({
  comments,
  deleteComment,
  handleComment,
  editing,
  setEditing
}: any) => {
  const handleDeleteComment = (ev: any) => {
    deleteComment(ev.target.id);
  };

  const handleUpdateComment = (ev: any) => {
    handleComment(ev.target.id);
    setEditing(true);
  };

  return (
    <section className="section-comments">
      <ul className="ul-comment">
        {comments.length !== 0
          ? comments.map((comment: any) => {
              return (
                <>
                  <li key={comment.id}>
                    <p>{comment.content}</p>
                    <Button onClick={handleDeleteComment} id={comment.id} className="btn-delete-commt">
                      delete
                    </Button>
                    <Button onClick={handleUpdateComment} id={comment.id} disabled={editing? true : false} className="btn-edit-commt">
                      edit
                    </Button>
                  </li>
                </>
              );
            })
          : "Not comments"}
      </ul>
    </section>
  );
};
