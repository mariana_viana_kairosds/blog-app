import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const StyledLink = styled(Link)`
text-decoration:none;
color: #09f;
margin: 1.5rem;
padding: 0.5rem;

&:hover{
    border-bottom: 2px solid #09f;
}
`