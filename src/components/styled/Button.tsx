import styled from 'styled-components';

export const Button = styled.button`
background: white;
cursor: pointer;
font-size: 1em;
margin:1em;
padding: 4px 12px;
border-radius: 5px;
transition: all .3s ease;
border: none;
box-shadow:2px 2px 3px 1px rgba(104, 213, 225, 0.727);

$:hover{
    background: #09f;
}
`