import styled from 'styled-components';

export const StyledSection = styled.section`
display: flex;
justify-content: right;
`

export const StyledForm = styled.form`
padding: 2rem;
width:100%;
display:flex;
flex-direction:column;
align-items: center;
`

export const StyledLabel = styled.label`
width:200px;
margin-top: 0.5rem;
color:rgb(6, 69, 60);
`

export const StyledSelect = styled.select`
width:200px;
margin-top: 0.5rem;
color:rgb(6, 69, 60);
`

export const StyledInput = styled.input`
width:200px;
border:none;
border-bottom: 1px solid black;
padding:0.3rem;
border-radius: 0.2rem;
background-color:transparent;
`

export const StyledMessage = styled.div`
color:rgb(6, 69, 60);
display:flex;
justify-content:center;
`