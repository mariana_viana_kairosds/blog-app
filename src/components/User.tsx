import { StyledInput, StyledLabel } from "./styled/generalStyled";

export const User = (props: any) => {

  const handleClick = (ev: any) => {
    props.handleUser(ev.target.value);
  };

  return (
    <>
      <StyledLabel htmlFor="name">Author</StyledLabel>
      <StyledInput
        type={props.defaultProps}
        id="name"
        onChange={handleClick}
        value={props.author}
      />
    </>
  );
};

User.defaultProps = {
  inputType: "text",
};
