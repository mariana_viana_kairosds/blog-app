import "../styles/Post.css";
import { Button } from "./styled/Button";
import { StyledForm, StyledInput, StyledLabel } from "./styled/generalStyled";

export const SignUpComp = ({ getValueSignUp }: any) => {
  const submitForm = (ev: any) => {
    ev.preventDefault();
    getValueSignUp({
      nickName: ev.currentTarget.children.nickName.value,
      email: ev.currentTarget.children.email.value,
      password: ev.currentTarget.children.password.value,
      role: ev.currentTarget.children.role.value,
    });
  };

  return (
    <StyledForm onSubmit={submitForm}>
      <StyledLabel htmlFor="nickName">NickName: </StyledLabel>
      <StyledInput
        type="text"
        name="nickName"
        id="nickName"
        autoComplete="off"
      />
      <StyledLabel htmlFor="email">Email: </StyledLabel>
      <StyledInput type="email" name="email" id="email" autoComplete="off" />
      <StyledLabel htmlFor="pass">Password:</StyledLabel>
      <StyledInput type="password" name="password" id="pass" />
      <select name="role" className="select-token">
      <option selected>Select Role</option>
      <option>USER</option>
      <option>AUTHOR</option>
    </select>
      <Button>Register</Button>
    </StyledForm>
  );
};
