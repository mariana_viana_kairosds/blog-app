import { StyledInput, StyledLabel } from "./styled/generalStyled";

export const Title = (props: any) => {

    const handleClick = (ev: any) => {
        props.handleTitle(ev.target.value)
    }

    return (
        <>
        <StyledLabel htmlFor="title">Title</StyledLabel>
        <StyledInput type={props.defaultProps} name="title" id="title" value={props.title} onChange={handleClick} />
    </>
    )

}

Title.defaultProps = {
  inputType: 'text'
};