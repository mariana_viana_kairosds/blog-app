import { StyledInput, StyledLabel } from "./styled/generalStyled";

export const Content = (props: any) => {

    const handleClick = (ev: any) => {
        props.handleContent(ev.target.value)
    }

    return (
        <>
        <StyledLabel htmlFor="content">Content</StyledLabel>
        <StyledInput type={props.defaultProps} id="content" onChange={handleClick} value={props.content}/>
    </>
    )
}

Content.defaultProps = {
  inputType: 'text'
};