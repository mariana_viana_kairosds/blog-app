import { Button } from "./styled/Button";
import { StyledForm, StyledInput, StyledLabel } from "./styled/generalStyled";


export const LoginComp = ({ getValueLogin }: any) => {
  const submitForm = (ev: any) => {
    ev.preventDefault();
    getValueLogin({
      email: ev.currentTarget.children.email.value,
      password: ev.currentTarget.children.password.value,
    });
  };

  return (
    <StyledForm onSubmit={submitForm}>
      <StyledLabel htmlFor="email">Email: </StyledLabel>
      <StyledInput type="email" name="email" id="email" autoComplete="off" placeholder="prueba@gmail.com" />
      <StyledLabel htmlFor="pass">Password:</StyledLabel>
      <StyledInput type="password" name="password" id="pass" />
      <Button>Login</Button>
    </StyledForm>
  );
};
