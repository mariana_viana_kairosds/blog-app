import { Content } from "./Content";
import { Button } from "./styled/Button";
import { StyledForm } from "./styled/generalStyled";
import { Title } from "./Title";
import { User } from "./User";

export const CreatePostForm = (props: any) => {
  return (
    <StyledForm onSubmit={(e) => e.preventDefault()}>
      <User handleUser={props.handleUser} author={props.author}></User>
      <Title handleTitle={props.handleTitle} title={props.title}></Title>
      <Content
        handleContent={props.handleContent}
        content={props.content}
      ></Content>
      <div>
        <Button
          className="btn-send"
          onClick={props.editing ? props.updatePosts : props.createHandle}
        >
          {props.editing ? "Update" : "Create"}
        </Button>
        <Button onClick={() => props.setClickButtonPost(false)}>Cancel</Button>
      </div>
    </StyledForm>
  );
};
