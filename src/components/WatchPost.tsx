import { useNavigate } from "react-router-dom";
import "../styles/Post.css";
import { Button } from "./styled/Button";

export const WatchPost = ({updateHandle, setEditing, deletePost, posts, btn}: any) => {

  const navigate = useNavigate();

  const removePostId = (ev: any) => {
      deletePost(ev.target.id);
  }

  const editPostId = (ev:any) => {
    setEditing(true);
    updateHandle(ev.target.id);
  }

  const seeDetail = async (ev:any) => {
     navigate(`/post/${ev.target.id}`);
  }

  const renderPosts = () => {
    return (
      <table>
        <thead>
          <tr>
            <th>title</th>
            <th>content</th>
          </tr>
        </thead>
        <tbody>
          {posts.map((post: any) => {
            return (
             <tr key={post.id}>
             <td>{post.title}</td>
             <td>{post.content}</td>
             <td>
             <Button onClick={removePostId} id={post.id} className="btn-delete" disabled={btn}>delete</Button>
             <Button id={post.id} onClick={editPostId} className="btn-edit" disabled={btn}>edit</Button>
             <Button id={post.id} onClick={seeDetail} className="btn-detail">detail</Button></td>
             </tr>
            )
          })}
        </tbody>
      </table>
    );
  };

  const renderPostsList = () => {
    return <article className="article-post">{renderPosts()}</article>;
  };

  const renderEmtyPost = () => {
    return <p>Not posts yet</p>;
  };

  return posts.length === 0 ? renderEmtyPost() : renderPostsList();
};
