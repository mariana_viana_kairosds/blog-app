import axios from "axios";
import { ConfigService } from "../services/config/config-service";
import { ModelLogin } from "../types/Model";


export class AuthRepository {
  async login({ email, password }: ModelLogin): Promise<string> {
    const { data } = await axios.post(`${ConfigService.env.api}/login`, {
      email,
      password,
    });
    return data.token;
  }

  /*async signUp({ email, password, role }: User): Promise<UserDTO> {
    const { data } = await axios.post(`${ConfigService.env.api}/auth/sign-up`, {
      email,
      password,
      role,
    });
    return data as UserDTO;
  }

  async getRole(): Promise<RoleDTO> {
    const { data } = await axios.get<RoleDTO>(
      `${ConfigService.env.API_URL}/auth/role/me`
    );
    return data;
  }*/

}
