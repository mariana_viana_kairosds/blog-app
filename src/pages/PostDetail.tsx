import { useNavigate, useParams } from "react-router-dom";
import { FormCreateComments } from "../components/FormCreateComment";
import { Notification } from "../components/Notification";
import { PostDetailComp } from "../components/PostDetail";
import { Button } from "../components/styled/Button";
import { useComments } from "../hooks/useComments";
import { useFetchPost } from "../hooks/useFetchPost";

export const PostDetail = () => {
  const navigate = useNavigate();
  const location = useParams();
  const { editing, setEditing} = useFetchPost();

  const {
    comments,
    author,
    content,
    handleContent,
    updateComment,
    handleUser,
    handleComment,
    deleteComment,
    createComment,
    message
  } = useComments(location);

  const renderNotification = () => {
    return <Notification message={message}></Notification>
  }

  return (
    <>
    <article>
      <div><Button onClick={() => navigate("/post")}>Back</Button></div>
      <FormCreateComments
          handleUser={handleUser}
          handleContent={handleContent}
          updateComment={updateComment}
          createComment={createComment}
          author={author}
          content={content}
          editing={editing}
        />
        {renderNotification()}
        <PostDetailComp
        editing={editing}
        setEditing={setEditing}
        comments={comments}
        deleteComment={deleteComment}
        handleComment={handleComment}
        />
    </article>
    </>
  );
};
