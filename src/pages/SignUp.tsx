import { Notification } from "../components/Notification";
import { SignUpComp } from "../components/SignUp-comp";
import { StyledMessage, StyledSection } from "../components/styled/generalStyled";
import { StyledLink } from "../components/styled/Link";
import { useNotification } from "../hooks/useNotification";
import { useUser } from "../hooks/useUser";
import { SignUpRepository } from "../services/sign-up";

import { ModelSignUp } from "../types/Model";

export const SignUp = () => {
  const { setSignUp , signUp} = useUser();
  const { errorMessage, setErrorMessage } = useNotification();

  const getValueSignUp = async (value: ModelSignUp) => {
    console.log(signUp);
    try {
      const response = await SignUpRepository({
        nickName: value.nickName,
        email: value.email,
        password: value.password,
        role: value.role
      });
      setSignUp(response.status);
    } catch (error: any) {
      setErrorMessage(error.response.data.error);
    }
  };

  return (
    <>
    <StyledSection className="section">
      <StyledLink to="/login">Login</StyledLink>
    </StyledSection>
      <SignUpComp getValueSignUp={getValueSignUp}></SignUpComp>
      <StyledMessage>
        <p>{signUp}</p>
      </StyledMessage>
      <Notification message={errorMessage}></Notification>
    </>
  );
};
