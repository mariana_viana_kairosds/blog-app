import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { CreatePostForm } from "../components/FormCreatePost";
import { Notification } from "../components/Notification";
import { Button } from "../components/styled/Button";
import { WatchPost } from "../components/WatchPost";
import { RoleContext } from "../context/role-context";
import { useFetchPost } from "../hooks/useFetchPost";
import { useNotification } from "../hooks/useNotification";
import "../styles/Post.css";

export const PostsPage = () => {
  const {
    setEditing,
    editing,
    posts,
    author,
    title,
    content,
    handleTitle,
    handleContent,
    handleUser,
    createPost,
    deletePost,
    updatePost,
    updatePosts,
    clickButtonPost,
    setClickButtonPost,
  } = useFetchPost();

  const ctrrole = useContext(RoleContext);

  const { errorMessage } = useNotification();
  const navigate = useNavigate();

  const btn = ctrrole?.role === "USER";

  return (
    <div className="App">
      {clickButtonPost === false ? (
        <div className="div-button">
          <Button
            onClick={() => {
              setClickButtonPost(true);
            }}
            disabled={btn}
            className="btn-create"
          >
            Crear Post
          </Button>
          <Button
            onClick={() => {
              window.localStorage.removeItem("tokenBlog");
              navigate("/login");
            }}
          >
            Logout
          </Button>
        </div>
      ) : (
        <section>
          <CreatePostForm
            handleUser={handleUser}
            handleTitle={handleTitle}
            handleContent={handleContent}
            author={author}
            title={title}
            content={content}
            editing={editing}
            createHandle={createPost}
            updatePosts={updatePosts}
            setClickButtonPost={setClickButtonPost}
          ></CreatePostForm>
          <Notification message={errorMessage}></Notification>
        </section>
      )}
      <section className="section-post">
        <WatchPost
          updateHandle={updatePost}
          setEditing={setEditing}
          deletePost={deletePost}
          posts={posts}
          btn={btn}
        />
      </section>
    </div>
  );
};
