import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { LoginComp } from "../components/Login-comp";
import { Notification } from "../components/Notification";
import { StyledSection } from "../components/styled/generalStyled";
import { StyledLink } from "../components/styled/Link";
import { RoleContext } from "../context/role-context";
import { useNotification } from "../hooks/useNotification";
import { GetRole } from "../services/getRole";
import { LoginRepository } from "../services/login";

import { ModelLogin } from "../types/Model";

export const Login = () => {
  const { setErrorMessage, errorMessage } = useNotification();
  const navigate = useNavigate();
  const role = useContext(RoleContext);

  const getValueLogin = async (value: ModelLogin) => {
    try {
      const response = await LoginRepository({
        email: value.email,
        password: value.password,
      });
      const newRole = await GetRole(response.token);
      role?.setRole(newRole.role);
      navigate("/post", { replace: true });
      window.localStorage.setItem("tokenBlog", JSON.stringify(response));
    } catch (error: any) {
      setErrorMessage(error.response.data.error);
    }
  };

  return (
    <>
    <StyledSection className="section">
      <StyledLink to="/sign-up">register</StyledLink>
    </StyledSection>
      <LoginComp getValueLogin={getValueLogin}></LoginComp>
      <Notification message={errorMessage}></Notification>
    </>
  );
};
